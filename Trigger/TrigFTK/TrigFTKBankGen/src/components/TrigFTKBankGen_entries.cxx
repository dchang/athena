/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

#include "TrigFTKBankGen/FTKBankGenAlgo.h"
#include "TrigFTKBankGen/FTKConstGenAlgo.h"
#include "TrigFTKBankGen/PattMergeRootAlgo.h"
#include "TrigFTKBankGen/FTKPattGenRootAlgo.h"
#include "TrigFTKBankGen/FTKCachedBankGenAlgo.h"
#include "TrigFTKBankGen/FTKDataMatrixFromMCAlgo.h"

DECLARE_COMPONENT( FTKBankGenAlgo )
DECLARE_COMPONENT( FTKConstGenAlgo )
DECLARE_COMPONENT( PattMergeRootAlgo )
DECLARE_COMPONENT( FTKPattGenRootAlgo )
DECLARE_COMPONENT( FTKCachedBankGenAlgo )
DECLARE_COMPONENT(FTKDataMatrixFromMCAlgo)
